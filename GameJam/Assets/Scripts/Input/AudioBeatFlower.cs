﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBeatFlower : AudioSyncer
{
    public GameObject beatFlower1;
    public GameObject beatFlower2;
    public GameObject AudioController;
  //  public GameObject beatFlower3;
    //public GameObject beatFlower4;
    private bool animating = false;

    public override void OnUpdate()
    {
        base.OnUpdate();

        StartCoroutine(AnimateFlower());

        if (beatFlower1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            beatFlower2.SetActive(true);
        else
            //Debug.Log("playing");

 
        if (m_isBeat) return;
        
    }

    private IEnumerator WitherFlower()
    {
        if (animating == true)
        {
            yield return new WaitForSeconds(1f);
            beatFlower1.GetComponent<Animator>().SetBool("Beat", false);
            //animating = false;
        }

    }


    //for (int i = 0; i < flower.Count; i++)
    //{
    //    if (flower[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
    //        flower[i].SetActive(false);
    //    else
    //        Debug.Log("playing");
    //}



    private IEnumerator AnimateFlower()
    {
        animating = true;
        yield return new WaitForSeconds(1f);
        beatFlower1.GetComponent<Animator>().SetBool("Beat", true);
    }
}

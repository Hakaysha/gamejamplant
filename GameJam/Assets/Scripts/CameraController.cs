using UnityEngine;

public class CameraController : MonoBehaviour {
	
	public Vector3 cameraPosition = Vector3.zero;
	public Transform target;
    public float cameraSpeed = 0.05f;
	
	void FixedUpdate() {
		
		cameraPosition = new Vector3(
			Mathf.SmoothStep(transform.position.x, target.transform.position.x, cameraSpeed),
			Mathf.SmoothStep(transform.position.y, target.transform.position.y, cameraSpeed));
		
	}
	
	void LateUpdate() {
		
		transform.position = cameraPosition + Vector3.forward * -10;
		
	}
	
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform plant;

    private void Update()
    {
        transform.position = new Vector3(plant.transform.position.x, plant.transform.position.y, plant.transform.position.z);
    }

}
